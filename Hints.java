import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Метод schedule класса Timer позволяет заплонировать выполнение какой-либо задачи в будущем,
 * при этом планируемое задание должно быть экземпляром TimerTask
 */
class RunProcessTimerTask extends TimerTask {
    private String processName = "";

    public void setProcess(String name) {
        processName = name;
    }

    public void run() {
        try {
            System.out.println("Запуск '" + processName + "'.");
            new ProcessBuilder(processName).start();
            System.exit(0);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

/**
 * Собрал методы для взаимодействия с датой-временем в отдельный класс
 */
class DateAndTime {
    private static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm");

    /**
     * Преобразует дату из строки в тип Date
     */
    public static Date parseDate(String date) throws ParseException {
        return dateFormat.parse(date);
    }

    /**
     * Возращает true, если date ещё не прошло, иначе false
     */
    public static boolean check(Date date) {
        Date currentDate = new Date();
        return date.after(currentDate);
    }

}

/**
 * Hints is not task scheduler
 * Простое приложение для отложенного запуска программ
 *
 * Created by ragweed on 31.01.17.
 */
public class Hints {

    // Если равно true, то приложение будет запущено в 1:05 AM следующего дня
    private static boolean megafon = false;

    /**
     * Обёртка над System.out.println() чтоб не писать многабукофф
     */
    public static void print(String out) {
        System.out.println(out);
    }

    /**
     * Вернёт строку содержащую в себе краткое описание, как работать с программой
     */
    public static String man() {
        String text = "Необходимо на стандартный ввод передать название приложения и время его запуска в формате" +
                " yyyy-mm-dd hh:mm, например: \njava hints kget 2016-02-07 01:05\n\n" +
                "Также, вместо даты-времени можно передавать некоторые аргументы:\n" +
                "\t-c, --check\t\tзапуск приложения без ожидания, позволяет проверить, запустится ли указанное " +
                "приложение\n" +
                "\t-m, --megafon\tзапуск приложения в 1:05 AM следующего дня";
        return text;
    }

    /**
     * Данный метод используется для вывода ошибки ввода
     */
    public static void errorIn(String errorInfo) {
        print(errorInfo + "\n" + man());
    }

    public static void about() {
        String text = "Hints is not task scheduler.\n\n" +
                "* А что это такое?\n" +
                "Простое приложение для отложенного запуска программ.\n\n" +
                "* Как пользоваться hints?\n" + man();
        print(text);
    }

    public static void main(String[] args) {
        // Получаем со стандартного ввода название запускаемого приложения
        if (args.length >= 1) {
            String processName = args[0];

            // Проверяем, была ли передана дата и время на стандартный ввод
            if (args.length < 3) {
                // Может быть были переданы какие-нибудь команды?
                if (args.length == 2) {
                    switch(args[1]){
                        // Проверка, запускается ли приложение
                        case "--check" :
                        case "-c":
                            print("Запускаю '" + processName + "'.");
                            try {
                                new ProcessBuilder(processName).start();
                            } catch (IOException e) {
                                print("Не удалось запустить приложение.");
                                e.printStackTrace();
                            }
                            System.exit(0);
                            break;
                        case "--megafon" :
                        case "-m":
                            megafon = true;
                            break;
                        default :
                            errorIn("Неизвестная команда '" + args[1] + "' или не передано время старта.");
                            System.exit(-1);
                    }
                } else {
                    errorIn("Не передано время старта.");
                    System.exit(-1);
                }
            }

            // Получаем со стандартного ввода дату и время в формате "yyyy-mm-dd hh:mm", и пытаемся её распарсить
            try {
                Date parsingDate;

                if (megafon == false) {
                    String startTime = args[1] + " " + args[2];
                    parsingDate = DateAndTime.parseDate(startTime);
                } else {
                    Calendar cal = new GregorianCalendar();
                    cal.set(Calendar.HOUR, 13);
                    cal.set(Calendar.MINUTE, 5);
                    cal.set(Calendar.SECOND, 0);
                    parsingDate = cal.getTime();
                }

                if (DateAndTime.check(parsingDate)) {
                    print("В '" + parsingDate + "' будет запущено приложение '" + processName + "'");
                    RunProcessTimerTask testTask = new RunProcessTimerTask();
                    testTask.setProcess(processName);
                    Timer myTimer = new Timer();
                    myTimer.schedule(testTask, parsingDate);
                } else {
                    print("Указанное время " + parsingDate + " уже прошло.");
                }
            } catch (ParseException e) {
                errorIn("Не удалось распарсить указанную дату-время. Проверте правильность ввода.");
            }
        } else {
            about();
        }
    }

}